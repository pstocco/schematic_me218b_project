EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Final Project Schematic"
Date "2022-03-07"
Rev ""
Comp "Team 1: P Stocco, K Huang, R Randall, B Choi"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L lab8_schematic-rescue:Cap-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue C7
U 1 1 61FB99A6
P 7550 3350
F 0 "C7" V 7298 3350 50  0000 C CNN
F 1 "10uF" V 7389 3350 50  0000 C CNN
F 2 "" H 7588 3200 50  0001 C CNN
F 3 "" H 7550 3350 50  0001 C CNN
	1    7550 3350
	0    1    1    0   
$EndComp
$Comp
L lab8_schematic-rescue:Cap-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue C6
U 1 1 61FB92F1
P 6850 2800
F 0 "C6" H 6965 2846 50  0000 L CNN
F 1 "0.1uF" H 6965 2755 50  0000 L CNN
F 2 "" H 6888 2650 50  0001 C CNN
F 3 "" H 6850 2800 50  0001 C CNN
	1    6850 2800
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+3.3V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR040
U 1 1 61FB881D
P 6850 2650
F 0 "#PWR040" H 6850 2500 50  0001 C CNN
F 1 "+3.3V" H 6865 2823 50  0000 C CNN
F 2 "" H 6850 2650 50  0001 C CNN
F 3 "" H 6850 2650 50  0001 C CNN
	1    6850 2650
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+3.3V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR038
U 1 1 61FB6E66
P 6850 1200
F 0 "#PWR038" H 6850 1050 50  0001 C CNN
F 1 "+3.3V" H 6865 1373 50  0000 C CNN
F 2 "" H 6850 1200 50  0001 C CNN
F 3 "" H 6850 1200 50  0001 C CNN
	1    6850 1200
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Cap-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue C5
U 1 1 61FB7D92
P 6850 1350
F 0 "C5" H 6965 1396 50  0000 L CNN
F 1 "0.1uF" H 6965 1305 50  0000 L CNN
F 2 "" H 6888 1200 50  0001 C CNN
F 3 "" H 6850 1350 50  0001 C CNN
	1    6850 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 1200 8400 600 
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR043
U 1 1 61F21ED7
P 7400 3350
F 0 "#PWR043" H 7400 3100 50  0001 C CNN
F 1 "GND" H 7405 3177 50  0000 C CNN
F 2 "" H 7400 3350 50  0001 C CNN
F 3 "" H 7400 3350 50  0001 C CNN
	1    7400 3350
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR041
U 1 1 61F22E4A
P 6850 2950
F 0 "#PWR041" H 6850 2700 50  0001 C CNN
F 1 "GND" H 6855 2777 50  0000 C CNN
F 2 "" H 6850 2950 50  0001 C CNN
F 3 "" H 6850 2950 50  0001 C CNN
	1    6850 2950
	1    0    0    -1  
$EndComp
Connection ~ 6850 2650
Connection ~ 6850 2950
Connection ~ 7400 3350
Connection ~ 6850 1200
$Comp
L lab8_schematic-rescue:PIC32MX170F256B-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue U7
U 1 1 61FB42A7
P 7900 2050
F 0 "U7" H 7625 3165 50  0000 C CNN
F 1 "PIC32MX170F256B" H 7625 3074 50  0000 C CNN
F 2 "" H 7900 2050 50  0001 C CNN
F 3 "" H 7900 2050 50  0001 C CNN
	1    7900 2050
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR039
U 1 1 61F2498F
P 6850 1500
F 0 "#PWR039" H 6850 1250 50  0001 C CNN
F 1 "GND" H 6855 1327 50  0000 C CNN
F 2 "" H 6850 1500 50  0001 C CNN
F 3 "" H 6850 1500 50  0001 C CNN
	1    6850 1500
	1    0    0    -1  
$EndComp
Connection ~ 6850 1500
$Comp
L lab8_schematic-rescue:Motor-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue M3
U 1 1 61F27FFF
P 2950 950
F 0 "M3" H 3097 946 50  0000 L CNN
F 1 "Motor" H 3097 855 50  0000 L CNN
F 2 "" H 2950 860 50  0001 C CNN
F 3 "" H 2950 860 50  0001 C CNN
	1    2950 950 
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR014
U 1 1 61F3DF39
P 2300 950
F 0 "#PWR014" H 2300 700 50  0001 C CNN
F 1 "GND" H 2305 777 50  0000 C CNN
F 2 "" H 2300 950 50  0001 C CNN
F 3 "" H 2300 950 50  0001 C CNN
	1    2300 950 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8650 1200 8400 1200
Connection ~ 8400 1200
$Comp
L lab8_schematic-rescue:+5V-power #PWR012
U 1 1 61FB2403
P 1900 6350
F 0 "#PWR012" H 1900 6200 50  0001 C CNN
F 1 "+5V" H 1915 6523 50  0000 C CNN
F 2 "" H 1900 6350 50  0001 C CNN
F 3 "" H 1900 6350 50  0001 C CNN
	1    1900 6350
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Photo_NPN LTR3208E1
U 1 1 61FB2418
P 800 6750
F 0 "LTR3208E1" H 991 6796 50  0000 L CNN
F 1 "Photo_NPN" H 991 6705 50  0000 L CNN
F 2 "" H 1000 6850 50  0001 C CNN
F 3 "" H 800 6750 50  0001 C CNN
	1    800  6750
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:MCP6294 U3
U 1 1 61FB244F
P 2000 6650
F 0 "U3" H 2344 6696 50  0000 L CNN
F 1 "MCP6294" H 2344 6605 50  0000 L CNN
F 2 "" H 1950 6750 50  0000 C CNN
F 3 "" H 2050 6850 50  0000 C CNN
	1    2000 6650
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:MCP6294 U3
U 3 1 61FACF76
P 9950 5750
F 0 "U3" H 10294 5796 50  0000 L CNN
F 1 "MCP6294" H 10294 5705 50  0000 L CNN
F 2 "" H 9900 5850 50  0000 C CNN
F 3 "" H 10000 5950 50  0000 C CNN
	3    9950 5750
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Cap C10
U 1 1 61FACF7C
P 8800 6000
F 0 "C10" H 8915 6046 50  0000 L CNN
F 1 "10uF" H 8915 5955 50  0000 L CNN
F 2 "" H 8838 5850 50  0001 C CNN
F 3 "" H 8800 6000 50  0001 C CNN
	1    8800 6000
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 R17
U 1 1 61FACF82
P 9250 5700
F 0 "R17" H 9318 5746 50  0000 L CNN
F 1 "10K" H 9318 5655 50  0000 L CNN
F 2 "" V 9290 5690 50  0001 C CNN
F 3 "" H 9250 5700 50  0001 C CNN
	1    9250 5700
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 R18
U 1 1 61FACF88
P 9250 6000
F 0 "R18" H 9318 6046 50  0000 L CNN
F 1 "10K" H 9318 5955 50  0000 L CNN
F 2 "" V 9290 5990 50  0001 C CNN
F 3 "" H 9250 6000 50  0001 C CNN
	1    9250 6000
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power #PWR053
U 1 1 61FACFA0
P 9850 5450
F 0 "#PWR053" H 9850 5300 50  0001 C CNN
F 1 "+5V" H 9865 5623 50  0000 C CNN
F 2 "" H 9850 5450 50  0001 C CNN
F 3 "" H 9850 5450 50  0001 C CNN
	1    9850 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 5850 9250 5850
Connection ~ 9250 5850
Wire Wire Line
	9250 5850 9650 5850
Wire Wire Line
	9650 5650 9500 5650
Wire Wire Line
	9500 5650 9500 5200
Wire Wire Line
	9500 5200 10250 5200
Wire Wire Line
	10250 5200 10250 5750
$Comp
L lab8_schematic-rescue:+5V-power #PWR050
U 1 1 61FACFB0
P 9250 5550
F 0 "#PWR050" H 9250 5400 50  0001 C CNN
F 1 "+5V" H 9265 5723 50  0000 C CNN
F 2 "" H 9250 5550 50  0001 C CNN
F 3 "" H 9250 5550 50  0001 C CNN
	1    9250 5550
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Cap C8
U 1 1 61FACFB6
P 7700 5850
F 0 "C8" H 7815 5896 50  0000 L CNN
F 1 "0.1uF" H 7815 5805 50  0000 L CNN
F 2 "" H 7738 5700 50  0001 C CNN
F 3 "" H 7700 5850 50  0001 C CNN
	1    7700 5850
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Cap C9
U 1 1 61FACFBC
P 8150 5850
F 0 "C9" H 8265 5896 50  0000 L CNN
F 1 "0.1uF" H 8265 5805 50  0000 L CNN
F 2 "" H 8188 5700 50  0001 C CNN
F 3 "" H 8150 5850 50  0001 C CNN
	1    8150 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 5700 8150 5450
Wire Wire Line
	8150 5450 7700 5450
Wire Wire Line
	7700 5450 7700 5700
Wire Wire Line
	7700 6000 7700 6250
Wire Wire Line
	7700 6250 8150 6250
Wire Wire Line
	8150 6250 8150 6000
$Comp
L lab8_schematic-rescue:+5V-power #PWR044
U 1 1 61FACFCE
P 7700 5450
F 0 "#PWR044" H 7700 5300 50  0001 C CNN
F 1 "+5V" H 7715 5623 50  0000 C CNN
F 2 "" H 7700 5450 50  0001 C CNN
F 3 "" H 7700 5450 50  0001 C CNN
	1    7700 5450
	1    0    0    -1  
$EndComp
Text Notes 7950 5400 0    50   ~ 0
Bypass capacitors\nat each IC
Connection ~ 7700 5450
$Comp
L ME218_BaseLib:Res1 R1
U 1 1 620C141C
P 1100 1100
F 0 "R1" H 1168 1146 50  0000 L CNN
F 1 "10k" H 1168 1055 50  0000 L CNN
F 2 "" V 1140 1090 50  0001 C CNN
F 3 "" H 1100 1100 50  0001 C CNN
	1    1100 1100
	1    0    0    -1  
$EndComp
Text Label 9700 1600 0    50   ~ 0
Blue
Text Label 9350 2450 0    50   ~ 0
Green
Text Label 9650 2350 0    50   ~ 0
Yellow
Wire Wire Line
	9350 2000 9350 2450
Wire Wire Line
	9700 600  9700 2050
$Comp
L lab8_schematic-rescue:+3.3V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR055
U 1 1 6205C362
P 10050 1950
F 0 "#PWR055" H 10050 1800 50  0001 C CNN
F 1 "+3.3V" H 10065 2123 50  0000 C CNN
F 2 "" H 10050 1950 50  0001 C CNN
F 3 "" H 10050 1950 50  0001 C CNN
	1    10050 1950
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+3.3V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR049
U 1 1 61FC6A07
P 8650 900
F 0 "#PWR049" H 8650 750 50  0001 C CNN
F 1 "+3.3V" H 8665 1073 50  0000 C CNN
F 2 "" H 8650 900 50  0001 C CNN
F 3 "" H 8650 900 50  0001 C CNN
	1    8650 900 
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Res1-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue R15
U 1 1 61FBE6F3
P 8650 1050
F 0 "R15" H 8718 1096 50  0000 L CNN
F 1 "10k" H 8718 1005 50  0000 L CNN
F 2 "" V 8690 1040 50  0001 C CNN
F 3 "" H 8650 1050 50  0001 C CNN
	1    8650 1050
	1    0    0    -1  
$EndComp
Connection ~ 4100 1550
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR025
U 1 1 620FD306
P 4100 1550
F 0 "#PWR025" H 4100 1300 50  0001 C CNN
F 1 "GND" H 4105 1377 50  0000 C CNN
F 2 "" H 4100 1550 50  0001 C CNN
F 3 "" H 4100 1550 50  0001 C CNN
	1    4100 1550
	1    0    0    -1  
$EndComp
Connection ~ 4100 2700
$Comp
L lab8_schematic-rescue:+3.3V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR026
U 1 1 620FD2D9
P 4100 2700
F 0 "#PWR026" H 4100 2550 50  0001 C CNN
F 1 "+3.3V" H 4115 2873 50  0000 C CNN
F 2 "" H 4100 2700 50  0001 C CNN
F 3 "" H 4100 2700 50  0001 C CNN
	1    4100 2700
	1    0    0    -1  
$EndComp
Connection ~ 4100 3000
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR027
U 1 1 620FD2F6
P 4100 3000
F 0 "#PWR027" H 4100 2750 50  0001 C CNN
F 1 "GND" H 4105 2827 50  0000 C CNN
F 2 "" H 4100 3000 50  0001 C CNN
F 3 "" H 4100 3000 50  0001 C CNN
	1    4100 3000
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Cap-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue C3
U 1 1 620FD2D3
P 4100 2850
F 0 "C3" H 4215 2896 50  0000 L CNN
F 1 "0.1uF" H 4215 2805 50  0000 L CNN
F 2 "" H 4138 2700 50  0001 C CNN
F 3 "" H 4100 2850 50  0001 C CNN
	1    4100 2850
	1    0    0    -1  
$EndComp
Connection ~ 4650 3400
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR030
U 1 1 620FD2F0
P 4650 3400
F 0 "#PWR030" H 4650 3150 50  0001 C CNN
F 1 "GND" H 4655 3227 50  0000 C CNN
F 2 "" H 4650 3400 50  0001 C CNN
F 3 "" H 4650 3400 50  0001 C CNN
	1    4650 3400
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Cap-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue C4
U 1 1 620FD2CD
P 4800 3400
F 0 "C4" V 4548 3400 50  0000 C CNN
F 1 "10uF" V 4639 3400 50  0000 C CNN
F 2 "" H 4838 3250 50  0001 C CNN
F 3 "" H 4800 3400 50  0001 C CNN
	1    4800 3400
	0    1    1    0   
$EndComp
Connection ~ 4100 1250
$Comp
L lab8_schematic-rescue:Cap-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue C2
U 1 1 620FD2E5
P 4100 1400
F 0 "C2" H 4215 1446 50  0000 L CNN
F 1 "0.1uF" H 4215 1355 50  0000 L CNN
F 2 "" H 4138 1250 50  0001 C CNN
F 3 "" H 4100 1400 50  0001 C CNN
	1    4100 1400
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+3.3V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR024
U 1 1 620FD2DF
P 4100 1250
F 0 "#PWR024" H 4100 1100 50  0001 C CNN
F 1 "+3.3V" H 4115 1423 50  0000 C CNN
F 2 "" H 4100 1250 50  0001 C CNN
F 3 "" H 4100 1250 50  0001 C CNN
	1    4100 1250
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:PIC32MX170F256B-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue U5
U 1 1 620FD300
P 5150 2100
F 0 "U5" H 4875 3215 50  0000 C CNN
F 1 "PIC32MX170F256B" H 4875 3124 50  0000 C CNN
F 2 "" H 5150 2100 50  0001 C CNN
F 3 "" H 5150 2100 50  0001 C CNN
	1    5150 2100
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Motor_Servo M2
U 1 1 620C9CA3
P 1450 3750
F 0 "M2" H 1782 3815 50  0000 L CNN
F 1 "Motor_Servo" H 1782 3724 50  0000 L CNN
F 2 "" H 1450 3560 50  0001 C CNN
F 3 "" H 1450 3560 50  0001 C CNN
	1    1450 3750
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Motor_Servo M1
U 1 1 620CB5DB
P 1400 3150
F 0 "M1" H 1732 3215 50  0000 L CNN
F 1 "Motor_Servo" H 1732 3124 50  0000 L CNN
F 2 "" H 1400 2960 50  0001 C CNN
F 3 "" H 1400 2960 50  0001 C CNN
	1    1400 3150
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power #PWR06
U 1 1 620D681D
P 1150 3750
F 0 "#PWR06" H 1150 3600 50  0001 C CNN
F 1 "+5V-power" V 1165 3877 50  0000 L CNN
F 2 "" H 1150 3750 50  0001 C CNN
F 3 "" H 1150 3750 50  0001 C CNN
	1    1150 3750
	0    -1   -1   0   
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power #PWR04
U 1 1 620D75FB
P 1100 3150
F 0 "#PWR04" H 1100 3000 50  0001 C CNN
F 1 "+5V-power" V 1115 3277 50  0000 L CNN
F 2 "" H 1100 3150 50  0001 C CNN
F 3 "" H 1100 3150 50  0001 C CNN
	1    1100 3150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 620D8126
P 1150 3850
F 0 "#PWR07" H 1150 3600 50  0001 C CNN
F 1 "GND" H 1155 3677 50  0000 C CNN
F 2 "" H 1150 3850 50  0001 C CNN
F 3 "" H 1150 3850 50  0001 C CNN
	1    1150 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 620D91A1
P 1100 3250
F 0 "#PWR05" H 1100 3000 50  0001 C CNN
F 1 "GND" H 1105 3077 50  0000 C CNN
F 2 "" H 1100 3250 50  0001 C CNN
F 3 "" H 1100 3250 50  0001 C CNN
	1    1100 3250
	1    0    0    -1  
$EndComp
Text GLabel 1100 3050 0    50   Input ~ 0
L_Brow_Servo
Text GLabel 1150 3650 0    50   Input ~ 0
R_Brow_Servo
Text GLabel 8400 1750 2    50   Input ~ 0
Dunking_Servo
Text GLabel 8400 2950 2    50   Input ~ 0
SCK
Text GLabel 8400 2350 2    50   Input ~ 0
MISO_SDI
Text GLabel 8400 2050 2    50   Input ~ 0
Beacon_In
Text GLabel 8400 2650 2    50   Input ~ 0
L_Brow_Servo
Text GLabel 8400 1850 2    50   Input ~ 0
R_Brow_Servo
Text GLabel 6850 1900 0    50   Input ~ 0
~SS
Text GLabel 5650 1800 2    50   Input ~ 0
1A
Text GLabel 5650 2400 2    50   Input ~ 0
MOSI_SDI
Text GLabel 5650 3000 2    50   Input ~ 0
SCK
Text GLabel 4100 2250 0    50   Input ~ 0
Encoder_M2
Text GLabel 5650 3100 2    50   Input ~ 0
3A
Text GLabel 4100 1950 0    50   Input ~ 0
~SS
$Comp
L lab8_schematic-rescue:Res1 R13
U 1 1 6218FAB1
P 5650 1100
F 0 "R13" H 5718 1146 50  0000 L CNN
F 1 "10k" H 5718 1055 50  0000 L CNN
F 2 "" V 5690 1090 50  0001 C CNN
F 3 "" H 5650 1100 50  0001 C CNN
	1    5650 1100
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+3.3V-power #PWR033
U 1 1 621A9CA6
P 5650 950
F 0 "#PWR033" H 5650 800 50  0001 C CNN
F 1 "+3.3V-power" H 5665 1123 50  0000 C CNN
F 2 "" H 5650 950 50  0001 C CNN
F 3 "" H 5650 950 50  0001 C CNN
	1    5650 950 
	1    0    0    -1  
$EndComp
Text GLabel 1500 2250 0    50   Input ~ 0
4A
Text GLabel 1100 2100 0    50   Input ~ 0
3A
Text GLabel 1100 950  0    50   Input ~ 0
1A
Text GLabel 1500 1100 0    50   Input ~ 0
2A
Text GLabel 1700 6750 0    50   Input ~ 0
Vref
Wire Wire Line
	10250 5750 10750 5750
Connection ~ 10250 5750
Text GLabel 10750 5750 2    50   Input ~ 0
Vref
Text GLabel 2850 4650 1    50   Input ~ 0
Front_US_Echo
Text GLabel 2950 4650 1    50   Input ~ 0
Front_US_Trig
Text GLabel 5650 2100 2    50   Input ~ 0
Front_US_Echo
Text GLabel 5650 2500 2    50   Input ~ 0
Front_US_Trig
Wire Notes Line
	3800 600  6300 600 
Wire Notes Line
	6300 600  6300 3700
Wire Notes Line
	6300 3700 3800 3700
Wire Notes Line
	3800 3700 3800 600 
Text Notes 3900 750  0    50   ~ 0
Follower
Wire Notes Line
	9000 3700 9000 550 
Wire Notes Line
	6400 550  6400 3700
Text Notes 6500 650  0    50   ~ 0
Leader
$Comp
L lab8_schematic-rescue:MulticolorLED D1
U 1 1 62239186
P 9450 3550
F 0 "D1" H 9850 3850 50  0000 R CNN
F 1 "MulticolorLED" H 10100 3950 50  0000 R CNN
F 2 "" V 9450 3300 50  0001 C CNN
F 3 "" V 9450 3300 50  0001 C CNN
	1    9450 3550
	-1   0    0    1   
$EndComp
$Comp
L lab8_schematic-rescue:MulticolorLED D2
U 1 1 6223B8A2
P 10300 3550
F 0 "D2" H 9922 3454 50  0000 R CNN
F 1 "MulticolorLED" H 9922 3545 50  0000 R CNN
F 2 "" V 10300 3300 50  0001 C CNN
F 3 "" V 10300 3300 50  0001 C CNN
	1    10300 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	10100 3150 10100 3050
Wire Wire Line
	10100 3050 9250 3050
Wire Wire Line
	9250 3050 9250 3150
Wire Wire Line
	9650 3150 9650 2950
Wire Wire Line
	9650 2950 10300 2950
$Comp
L power:GND #PWR052
U 1 1 6224AF8F
P 9450 3850
F 0 "#PWR052" H 9450 3600 50  0001 C CNN
F 1 "GND" H 9455 3677 50  0000 C CNN
F 2 "" H 9450 3850 50  0001 C CNN
F 3 "" H 9450 3850 50  0001 C CNN
	1    9450 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR057
U 1 1 6224C024
P 10300 3850
F 0 "#PWR057" H 10300 3600 50  0001 C CNN
F 1 "GND" H 10305 3677 50  0000 C CNN
F 2 "" H 10300 3850 50  0001 C CNN
F 3 "" H 10300 3850 50  0001 C CNN
	1    10300 3850
	1    0    0    -1  
$EndComp
Text GLabel 9250 2750 1    50   Input ~ 0
Blue_Eyes_Out
Wire Wire Line
	9350 2450 10150 2450
Wire Wire Line
	9450 2350 10150 2350
Wire Wire Line
	10150 2250 10050 2250
Wire Wire Line
	10050 2150 10150 2150
Wire Wire Line
	10150 2050 9700 2050
Text GLabel 10300 2650 2    50   Input ~ 0
Red_Eyes_Out
Text GLabel 6850 2200 0    50   Input ~ 0
Blue_Eyes_Out
Text GLabel 6850 2300 0    50   Input ~ 0
Red_Eyes_Out
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR056
U 1 1 61F267AE
P 10050 2550
F 0 "#PWR056" H 10050 2300 50  0001 C CNN
F 1 "GND" H 10055 2377 50  0000 C CNN
F 2 "" H 10050 2550 50  0001 C CNN
F 3 "" H 10050 2550 50  0001 C CNN
	1    10050 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 2950 10500 3150
$Comp
L lab8_schematic-rescue:Res1 R19
U 1 1 6204F907
P 10300 2800
F 0 "R19" H 10368 2846 50  0000 L CNN
F 1 "100" H 10368 2755 50  0000 L CNN
F 2 "" V 10340 2790 50  0001 C CNN
F 3 "" H 10300 2800 50  0001 C CNN
	1    10300 2800
	1    0    0    -1  
$EndComp
Connection ~ 10300 2950
Wire Wire Line
	10300 2950 10500 2950
$Comp
L lab8_schematic-rescue:MPLAB_Snap-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue PRG1
U 1 1 620517DF
P 10600 2250
F 0 "PRG1" H 10500 2700 50  0000 L CNN
F 1 "MPLAB_Snap" H 10400 2600 50  0000 L CNN
F 2 "" H 10600 2250 50  0001 C CNN
F 3 "" H 10600 2250 50  0001 C CNN
	1    10600 2250
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Res1 R16
U 1 1 62056246
P 9250 2900
F 0 "R16" H 9318 2946 50  0000 L CNN
F 1 "10R" H 9318 2855 50  0000 L CNN
F 2 "" V 9290 2890 50  0001 C CNN
F 3 "" H 9250 2900 50  0001 C CNN
	1    9250 2900
	1    0    0    -1  
$EndComp
Connection ~ 9250 3050
Text GLabel 4100 2350 0    50   Input ~ 0
Encoder_M1
Text GLabel 5650 1600 2    50   Input ~ 0
SNAP_YELLOW
Text GLabel 5650 1700 2    50   Input ~ 0
SNAP_GREEN
Text GLabel 5650 2200 2    50   Input ~ 0
TX
Text GLabel 5650 2300 2    50   Input ~ 0
RX
Text GLabel 8400 2150 2    50   Input ~ 0
TX
Text GLabel 8400 2250 2    50   Input ~ 0
RX
Text GLabel 6850 2100 0    50   Input ~ 0
MOSI_SDO
Text GLabel 4100 2150 0    50   Input ~ 0
MISO_SDO
Text GLabel 8400 2750 2    50   Input ~ 0
Beacon_AN_R
Text GLabel 8400 2850 2    50   Input ~ 0
Beacon_AN_L
Text GLabel 4100 2050 0    50   Input ~ 0
4A
Text GLabel 5650 2600 2    50   Input ~ 0
2A
Text GLabel 8400 2450 2    50   Input ~ 0
Load_Button
Text GLabel 8400 2550 2    50   Input ~ 0
Dunk_Button
Text GLabel 5650 1900 2    50   Input ~ 0
Back_US_Echo
Text GLabel 5650 2000 2    50   Input ~ 0
Back_US_Trig
$Comp
L lab8_schematic-rescue:HC-SR04 UltrasonicSensor1
U 1 1 621332AF
P 1600 4900
F 0 "UltrasonicSensor1" H 1272 4846 50  0000 R CNN
F 1 "HC-SR04" H 1272 4937 50  0000 R CNN
F 2 "" H 1100 5150 50  0001 C CNN
F 3 "" H 1100 5150 50  0001 C CNN
	1    1600 4900
	-1   0    0    1   
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power #PWR011
U 1 1 621332B5
P 1800 4500
F 0 "#PWR011" H 1800 4350 50  0001 C CNN
F 1 "+5V-power" H 1815 4673 50  0000 C CNN
F 2 "" H 1800 4500 50  0001 C CNN
F 3 "" H 1800 4500 50  0001 C CNN
	1    1800 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 621332BB
P 1700 4650
F 0 "#PWR010" H 1700 4400 50  0001 C CNN
F 1 "GND" H 1705 4477 50  0000 C CNN
F 2 "" H 1700 4650 50  0001 C CNN
F 3 "" H 1700 4650 50  0001 C CNN
	1    1700 4650
	-1   0    0    1   
$EndComp
Text GLabel 1400 4650 1    50   Input ~ 0
Back_US_Echo
Text GLabel 1500 4650 1    50   Input ~ 0
Back_US_Trig
Wire Wire Line
	8400 600  9700 600 
Wire Wire Line
	9100 1550 9100 1900
Wire Wire Line
	9100 1900 9450 1900
Wire Wire Line
	8400 1550 9100 1550
Wire Wire Line
	9000 2000 9000 1650
Wire Wire Line
	8400 1650 9000 1650
Wire Wire Line
	9000 2000 9350 2000
Wire Wire Line
	9450 1900 9450 2350
Text GLabel 5650 2700 2    50   Input ~ 0
Front_Switch
Text GLabel 5650 2800 2    50   Input ~ 0
Back_Switch
$Comp
L finalproject_schematic-rescue:TLE5206-ME218_BaseLib U1
U 1 1 621C2CD0
P 1950 600
F 0 "U1" H 1975 665 50  0000 C CNN
F 1 "TLE5206" H 1975 574 50  0000 C CNN
F 2 "" H 1950 600 50  0001 C CNN
F 3 "" H 1950 600 50  0001 C CNN
	1    1950 600 
	1    0    0    -1  
$EndComp
NoConn ~ 1650 800 
Wire Wire Line
	2300 1100 2650 1100
Text Notes 2450 1100 0    50   ~ 0
14V
Wire Wire Line
	2950 750  2300 750 
Wire Wire Line
	2300 750  2300 800 
Wire Wire Line
	2300 1250 2950 1250
$Comp
L ME218_BaseLib:Res1 R3
U 1 1 621EA5D4
P 1500 1250
F 0 "R3" H 1568 1296 50  0000 L CNN
F 1 "10k" H 1568 1205 50  0000 L CNN
F 2 "" V 1540 1240 50  0001 C CNN
F 3 "" H 1500 1250 50  0001 C CNN
	1    1500 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1100 1500 1100
Wire Wire Line
	1650 950  1100 950 
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR08
U 1 1 62206014
P 1500 1400
F 0 "#PWR08" H 1500 1150 50  0001 C CNN
F 1 "GND" H 1505 1227 50  0000 C CNN
F 2 "" H 1500 1400 50  0001 C CNN
F 3 "" H 1500 1400 50  0001 C CNN
	1    1500 1400
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR02
U 1 1 6220999B
P 1100 1250
F 0 "#PWR02" H 1100 1000 50  0001 C CNN
F 1 "GND" H 1105 1077 50  0000 C CNN
F 2 "" H 1100 1250 50  0001 C CNN
F 3 "" H 1100 1250 50  0001 C CNN
	1    1100 1250
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Motor-ME218_BaseLib-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue M4
U 1 1 622133A9
P 2950 2100
F 0 "M4" H 3097 2096 50  0000 L CNN
F 1 "Motor" H 3097 2005 50  0000 L CNN
F 2 "" H 2950 2010 50  0001 C CNN
F 3 "" H 2950 2010 50  0001 C CNN
	1    2950 2100
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR015
U 1 1 622133AF
P 2300 2100
F 0 "#PWR015" H 2300 1850 50  0001 C CNN
F 1 "GND" H 2305 1927 50  0000 C CNN
F 2 "" H 2300 2100 50  0001 C CNN
F 3 "" H 2300 2100 50  0001 C CNN
	1    2300 2100
	0    -1   -1   0   
$EndComp
$Comp
L ME218_BaseLib:Res1 R2
U 1 1 622133B5
P 1100 2250
F 0 "R2" H 1168 2296 50  0000 L CNN
F 1 "10k" H 1168 2205 50  0000 L CNN
F 2 "" V 1140 2240 50  0001 C CNN
F 3 "" H 1100 2250 50  0001 C CNN
	1    1100 2250
	1    0    0    -1  
$EndComp
$Comp
L finalproject_schematic-rescue:TLE5206-ME218_BaseLib U2
U 1 1 622133BD
P 1950 1750
F 0 "U2" H 1975 1815 50  0000 C CNN
F 1 "TLE5206" H 1975 1724 50  0000 C CNN
F 2 "" H 1950 1750 50  0001 C CNN
F 3 "" H 1950 1750 50  0001 C CNN
	1    1950 1750
	1    0    0    -1  
$EndComp
NoConn ~ 1650 1950
Wire Wire Line
	2300 2250 2650 2250
Text Notes 2450 2250 0    50   ~ 0
14V
Wire Wire Line
	2950 1900 2300 1900
Wire Wire Line
	2300 1900 2300 1950
Wire Wire Line
	2300 2400 2950 2400
$Comp
L ME218_BaseLib:Res1 R4
U 1 1 622133C9
P 1500 2400
F 0 "R4" H 1568 2446 50  0000 L CNN
F 1 "10k" H 1568 2355 50  0000 L CNN
F 2 "" V 1540 2390 50  0001 C CNN
F 3 "" H 1500 2400 50  0001 C CNN
	1    1500 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2250 1500 2250
Wire Wire Line
	1650 2100 1100 2100
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR09
U 1 1 622133D1
P 1500 2550
F 0 "#PWR09" H 1500 2300 50  0001 C CNN
F 1 "GND" H 1505 2377 50  0000 C CNN
F 2 "" H 1500 2550 50  0001 C CNN
F 3 "" H 1500 2550 50  0001 C CNN
	1    1500 2550
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR03
U 1 1 622133D7
P 1100 2400
F 0 "#PWR03" H 1100 2150 50  0001 C CNN
F 1 "GND" H 1105 2227 50  0000 C CNN
F 2 "" H 1100 2400 50  0001 C CNN
F 3 "" H 1100 2400 50  0001 C CNN
	1    1100 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 4500 1800 4650
$Comp
L lab8_schematic-rescue:HC-SR04 UltrasonicSensor2
U 1 1 6205942B
P 3050 4900
F 0 "UltrasonicSensor2" H 2722 4846 50  0000 R CNN
F 1 "HC-SR04" H 2722 4937 50  0000 R CNN
F 2 "" H 2550 5150 50  0001 C CNN
F 3 "" H 2550 5150 50  0001 C CNN
	1    3050 4900
	-1   0    0    1   
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power #PWR019
U 1 1 62288AFC
P 3250 4500
F 0 "#PWR019" H 3250 4350 50  0001 C CNN
F 1 "+5V-power" H 3265 4673 50  0000 C CNN
F 2 "" H 3250 4500 50  0001 C CNN
F 3 "" H 3250 4500 50  0001 C CNN
	1    3250 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 62288B02
P 3150 4650
F 0 "#PWR018" H 3150 4400 50  0001 C CNN
F 1 "GND" H 3155 4477 50  0000 C CNN
F 2 "" H 3150 4650 50  0001 C CNN
F 3 "" H 3150 4650 50  0001 C CNN
	1    3150 4650
	-1   0    0    1   
$EndComp
Wire Wire Line
	3250 4500 3250 4650
$Comp
L ME218_BaseLib:SW-DPDT SW1
U 1 1 6228D5DD
P 2850 2850
F 0 "SW1" H 2850 3135 50  0000 C CNN
F 1 "SW-DPDT" H 2850 3044 50  0000 C CNN
F 2 "" H 2850 2850 50  0001 C CNN
F 3 "" H 2850 2850 50  0001 C CNN
	1    2850 2850
	1    0    0    -1  
$EndComp
Text GLabel 2650 2850 0    50   Input ~ 0
Front_Switch
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR016
U 1 1 62296E9F
P 3050 2750
F 0 "#PWR016" H 3050 2500 50  0001 C CNN
F 1 "GND" H 3055 2577 50  0000 C CNN
F 2 "" H 3050 2750 50  0001 C CNN
F 3 "" H 3050 2750 50  0001 C CNN
	1    3050 2750
	0    -1   -1   0   
$EndComp
$Comp
L ME218_BaseLib:SW-DPDT SW1
U 2 1 62299AAA
P 2850 3400
F 0 "SW1" H 2850 3685 50  0000 C CNN
F 1 "SW-DPDT" H 2850 3594 50  0000 C CNN
F 2 "" H 2850 3400 50  0001 C CNN
F 3 "" H 2850 3400 50  0001 C CNN
	2    2850 3400
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR017
U 1 1 62299AB6
P 3050 3300
F 0 "#PWR017" H 3050 3050 50  0001 C CNN
F 1 "GND" H 3055 3127 50  0000 C CNN
F 2 "" H 3050 3300 50  0001 C CNN
F 3 "" H 3050 3300 50  0001 C CNN
	1    3050 3300
	0    -1   -1   0   
$EndComp
Text GLabel 2650 3400 0    50   Input ~ 0
Back_Switch
Text GLabel 2850 5900 0    50   Input ~ 0
Vref
Wire Wire Line
	3200 6450 3350 6450
Wire Wire Line
	3200 5900 3200 6450
$Comp
L lab8_schematic-rescue:+5V-power #PWR020
U 1 1 61FB247F
P 3550 6250
F 0 "#PWR020" H 3550 6100 50  0001 C CNN
F 1 "+5V" H 3565 6423 50  0000 C CNN
F 2 "" H 3550 6250 50  0001 C CNN
F 3 "" H 3550 6250 50  0001 C CNN
	1    3550 6250
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:MCP6294 U3
U 2 1 61FB2471
P 3650 6550
F 0 "U3" H 3994 6596 50  0000 L CNN
F 1 "MCP6294" H 3994 6505 50  0000 L CNN
F 2 "" H 3600 6650 50  0000 C CNN
F 3 "" H 3700 6750 50  0000 C CNN
	2    3650 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 6650 2300 6650
$Comp
L lab8_schematic-rescue:Quadrature_Encoder U6
U 1 1 6211A566
P 6000 4300
F 0 "U6" H 6528 4346 50  0000 L CNN
F 1 "Quadrature_Encoder" H 6528 4255 50  0000 L CNN
F 2 "" H 5900 4300 50  0001 C CNN
F 3 "" H 5900 4300 50  0001 C CNN
	1    6000 4300
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Quadrature_Encoder U8
U 1 1 62121C9F
P 8200 4300
F 0 "U8" H 8728 4346 50  0000 L CNN
F 1 "Quadrature_Encoder" H 8728 4255 50  0000 L CNN
F 2 "" H 8100 4300 50  0001 C CNN
F 3 "" H 8100 4300 50  0001 C CNN
	1    8200 4300
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR034
U 1 1 6212787D
P 5700 4150
F 0 "#PWR034" H 5700 4000 50  0001 C CNN
F 1 "+5V" H 5715 4323 50  0000 C CNN
F 2 "" H 5700 4150 50  0001 C CNN
F 3 "" H 5700 4150 50  0001 C CNN
	1    5700 4150
	0    -1   -1   0   
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR046
U 1 1 62137A49
P 7900 4150
F 0 "#PWR046" H 7900 4000 50  0001 C CNN
F 1 "+5V" H 7915 4323 50  0000 C CNN
F 2 "" H 7900 4150 50  0001 C CNN
F 3 "" H 7900 4150 50  0001 C CNN
	1    7900 4150
	0    -1   -1   0   
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR035
U 1 1 62147357
P 5700 4450
F 0 "#PWR035" H 5700 4200 50  0001 C CNN
F 1 "GND" H 5705 4277 50  0000 C CNN
F 2 "" H 5700 4450 50  0001 C CNN
F 3 "" H 5700 4450 50  0001 C CNN
	1    5700 4450
	0    1    1    0   
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR047
U 1 1 62151D25
P 7900 4450
F 0 "#PWR047" H 7900 4200 50  0001 C CNN
F 1 "GND" H 7905 4277 50  0000 C CNN
F 2 "" H 7900 4450 50  0001 C CNN
F 3 "" H 7900 4450 50  0001 C CNN
	1    7900 4450
	0    1    1    0   
$EndComp
NoConn ~ 7900 4350
NoConn ~ 5700 4350
$Comp
L lab8_schematic-rescue:Motor_Servo M5
U 1 1 62189204
P 4350 4300
F 0 "M5" H 4682 4365 50  0000 L CNN
F 1 "Motor_Servo" H 4682 4274 50  0000 L CNN
F 2 "" H 4350 4110 50  0001 C CNN
F 3 "" H 4350 4110 50  0001 C CNN
	1    4350 4300
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR022
U 1 1 6218A867
P 4050 4300
F 0 "#PWR022" H 4050 4150 50  0001 C CNN
F 1 "+5V" H 4065 4473 50  0000 C CNN
F 2 "" H 4050 4300 50  0001 C CNN
F 3 "" H 4050 4300 50  0001 C CNN
	1    4050 4300
	0    -1   -1   0   
$EndComp
$Comp
L lab8_schematic-rescue:GND-power-lab7_schematic-rescue-lab8_schematic-rescue-lab8_schematic-rescue #PWR023
U 1 1 621A9EB1
P 4050 4400
F 0 "#PWR023" H 4050 4150 50  0001 C CNN
F 1 "GND" H 4055 4227 50  0000 C CNN
F 2 "" H 4050 4400 50  0001 C CNN
F 3 "" H 4050 4400 50  0001 C CNN
	1    4050 4400
	0    1    1    0   
$EndComp
Text GLabel 4050 4200 0    50   Input ~ 0
Dunking_Servo
Text GLabel 5700 4250 0    50   Input ~ 0
Encoder_M1
Text GLabel 7900 4250 0    50   Input ~ 0
Encoder_M2
$Comp
L ME218_BaseLib:SW-PB SW3
U 1 1 621090A1
P 7000 5250
F 0 "SW3" H 7000 5535 50  0000 C CNN
F 1 "SW-PB" H 7000 5444 50  0000 C CNN
F 2 "" H 7000 5450 50  0001 C CNN
F 3 "" H 7000 5450 50  0001 C CNN
	1    7000 5250
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Res1 R14
U 1 1 62111178
P 6600 5100
F 0 "R14" H 6668 5146 50  0000 L CNN
F 1 "10k" H 6668 5055 50  0000 L CNN
F 2 "" V 6640 5090 50  0001 C CNN
F 3 "" H 6600 5100 50  0001 C CNN
	1    6600 5100
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+3.3V-power #PWR037
U 1 1 621183A6
P 6600 4950
F 0 "#PWR037" H 6600 4800 50  0001 C CNN
F 1 "+3.3V-power" H 6615 5123 50  0000 C CNN
F 2 "" H 6600 4950 50  0001 C CNN
F 3 "" H 6600 4950 50  0001 C CNN
	1    6600 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR042
U 1 1 62119365
P 7200 5250
F 0 "#PWR042" H 7200 5000 50  0001 C CNN
F 1 "GND" H 7205 5077 50  0000 C CNN
F 2 "" H 7200 5250 50  0001 C CNN
F 3 "" H 7200 5250 50  0001 C CNN
	1    7200 5250
	1    0    0    -1  
$EndComp
Text GLabel 6600 5250 0    50   Input ~ 0
Dunk_Button
$Comp
L ME218_BaseLib:SW-PB SW2
U 1 1 62128F36
P 5650 5250
F 0 "SW2" H 5650 5535 50  0000 C CNN
F 1 "SW-PB" H 5650 5444 50  0000 C CNN
F 2 "" H 5650 5450 50  0001 C CNN
F 3 "" H 5650 5450 50  0001 C CNN
	1    5650 5250
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:Res1 R12
U 1 1 62128F3C
P 5250 5100
F 0 "R12" H 5318 5146 50  0000 L CNN
F 1 "10k" H 5318 5055 50  0000 L CNN
F 2 "" V 5290 5090 50  0001 C CNN
F 3 "" H 5250 5100 50  0001 C CNN
	1    5250 5100
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+3.3V-power #PWR032
U 1 1 62128F42
P 5250 4950
F 0 "#PWR032" H 5250 4800 50  0001 C CNN
F 1 "+3.3V-power" H 5265 5123 50  0000 C CNN
F 2 "" H 5250 4950 50  0001 C CNN
F 3 "" H 5250 4950 50  0001 C CNN
	1    5250 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR036
U 1 1 62128F48
P 5850 5250
F 0 "#PWR036" H 5850 5000 50  0001 C CNN
F 1 "GND" H 5855 5077 50  0000 C CNN
F 2 "" H 5850 5250 50  0001 C CNN
F 3 "" H 5850 5250 50  0001 C CNN
	1    5850 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 5250 5450 5250
Text GLabel 5250 5250 0    50   Input ~ 0
Load_Button
$Comp
L ME218_BaseLib:Res1 R6
U 1 1 61FB245F
P 2950 6800
F 0 "R6" H 3018 6846 50  0000 L CNN
F 1 "10K" H 3018 6755 50  0000 L CNN
F 2 "" V 2990 6790 50  0001 C CNN
F 3 "" H 2950 6800 50  0001 C CNN
	1    2950 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  6550 1450 6550
$Comp
L ME218_BaseLib:Res1 R5
U 1 1 62294733
P 1900 5800
F 0 "R5" V 1695 5800 50  0000 C CNN
F 1 "10k" V 1786 5800 50  0000 C CNN
F 2 "" V 1940 5790 50  0001 C CNN
F 3 "" H 1900 5800 50  0001 C CNN
	1    1900 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 5800 2050 5800
Wire Wire Line
	2300 5800 2300 6650
Wire Wire Line
	1750 5800 1450 5800
Wire Wire Line
	1450 5800 1450 6550
Connection ~ 1450 6550
$Comp
L power:GND #PWR01
U 1 1 6229B8F6
P 900 6950
F 0 "#PWR01" H 900 6700 50  0001 C CNN
F 1 "GND" H 905 6777 50  0000 C CNN
F 2 "" H 900 6950 50  0001 C CNN
F 3 "" H 900 6950 50  0001 C CNN
	1    900  6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 6229F269
P 1900 6950
F 0 "#PWR013" H 1900 6700 50  0001 C CNN
F 1 "GND" H 1905 6777 50  0000 C CNN
F 2 "" H 1900 6950 50  0001 C CNN
F 3 "" H 1900 6950 50  0001 C CNN
	1    1900 6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 6229FED6
P 3550 6850
F 0 "#PWR021" H 3550 6600 50  0001 C CNN
F 1 "GND" H 3555 6677 50  0000 C CNN
F 2 "" H 3550 6850 50  0001 C CNN
F 3 "" H 3550 6850 50  0001 C CNN
	1    3550 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 5900 2900 5900
Text GLabel 2950 7050 0    50   Input ~ 0
Vref
Wire Wire Line
	2950 6950 2950 7050
$Comp
L ME218_BaseLib:Res1 R7
U 1 1 622CA247
P 3050 5900
F 0 "R7" V 2845 5900 50  0000 C CNN
F 1 "1k" V 2936 5900 50  0000 C CNN
F 2 "" V 3090 5890 50  0001 C CNN
F 3 "" H 3050 5900 50  0001 C CNN
	1    3050 5900
	0    1    1    0   
$EndComp
Connection ~ 3200 5900
$Comp
L ME218_BaseLib:Res1 R8
U 1 1 622CB242
P 3350 5900
F 0 "R8" V 3555 5900 50  0000 C CNN
F 1 "3.3k" V 3464 5900 50  0000 C CNN
F 2 "" V 3390 5890 50  0001 C CNN
F 3 "" H 3350 5900 50  0001 C CNN
	1    3350 5900
	0    -1   -1   0   
$EndComp
$Comp
L lab8_schematic-rescue:Cap C1
U 1 1 622CCEB8
P 2800 6650
F 0 "C1" V 2548 6650 50  0000 C CNN
F 1 "10uF" V 2639 6650 50  0000 C CNN
F 2 "" H 2838 6500 50  0001 C CNN
F 3 "" H 2800 6650 50  0001 C CNN
	1    2800 6650
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 5900 3950 6550
Wire Wire Line
	3500 5900 3950 5900
$Comp
L lab8_schematic-rescue:+3.3V-power #PWR031
U 1 1 61FB241E
P 5000 5850
F 0 "#PWR031" H 5000 5700 50  0001 C CNN
F 1 "+3.3V" H 5015 6023 50  0000 C CNN
F 2 "" H 5000 5850 50  0001 C CNN
F 3 "" H 5000 5850 50  0001 C CNN
	1    5000 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5850 5000 5950
$Comp
L ME218_BaseLib:Res1 R11
U 1 1 61FB243B
P 5000 6100
F 0 "R11" H 5068 6146 50  0000 L CNN
F 1 "3.3k" H 5068 6055 50  0000 L CNN
F 2 "" V 5040 6090 50  0001 C CNN
F 3 "" H 5000 6100 50  0001 C CNN
	1    5000 6100
	1    0    0    -1  
$EndComp
$Comp
L lab8_schematic-rescue:+5V-power #PWR028
U 1 1 61FB2448
P 4600 6150
F 0 "#PWR028" H 4600 6000 50  0001 C CNN
F 1 "+5V" H 4615 6323 50  0000 C CNN
F 2 "" H 4600 6150 50  0001 C CNN
F 3 "" H 4600 6150 50  0001 C CNN
	1    4600 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 6250 5000 6450
Connection ~ 5000 6450
$Comp
L ME218_BaseLib:MCP6546 U4
U 1 1 61FB2498
P 4700 6450
F 0 "U4" H 5044 6496 50  0000 L CNN
F 1 "MCP6546" H 5044 6405 50  0000 L CNN
F 2 "" H 4650 6550 50  0001 C CNN
F 3 "" H 4750 6650 50  0001 C CNN
	1    4700 6450
	1    0    0    -1  
$EndComp
Text GLabel 5500 6450 2    50   Input ~ 0
Beacon_In
Wire Wire Line
	5000 6450 5500 6450
$Comp
L power:GND #PWR029
U 1 1 622A0042
P 4600 6750
F 0 "#PWR029" H 4600 6500 50  0001 C CNN
F 1 "GND" H 4605 6577 50  0000 C CNN
F 2 "" H 4600 6750 50  0001 C CNN
F 3 "" H 4600 6750 50  0001 C CNN
	1    4600 6750
	1    0    0    -1  
$EndComp
$Comp
L ME218_BaseLib:Res1 R9
U 1 1 622B74CE
P 4200 7500
F 0 "R9" V 3995 7500 50  0000 C CNN
F 1 "10k" V 4086 7500 50  0000 C CNN
F 2 "" V 4240 7490 50  0001 C CNN
F 3 "" H 4200 7500 50  0001 C CNN
	1    4200 7500
	0    1    1    0   
$EndComp
$Comp
L ME218_BaseLib:Res1 R10
U 1 1 622BA812
P 4600 7500
F 0 "R10" V 4395 7500 50  0000 C CNN
F 1 "100k" V 4486 7500 50  0000 C CNN
F 2 "" V 4640 7490 50  0001 C CNN
F 3 "" H 4600 7500 50  0001 C CNN
	1    4600 7500
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 7500 5000 7500
Wire Wire Line
	5000 7500 5000 6450
Text GLabel 4050 7500 0    50   Input ~ 0
Vref
Wire Wire Line
	3950 6550 4400 6550
Connection ~ 3950 6550
Wire Wire Line
	3350 6650 2950 6650
Connection ~ 2950 6650
Wire Wire Line
	1450 6550 1700 6550
Connection ~ 2300 6650
Wire Wire Line
	6600 5250 6800 5250
$Comp
L power:GND #PWR045
U 1 1 62360D3B
P 7700 6250
F 0 "#PWR045" H 7700 6000 50  0001 C CNN
F 1 "GND" H 7705 6077 50  0000 C CNN
F 2 "" H 7700 6250 50  0001 C CNN
F 3 "" H 7700 6250 50  0001 C CNN
	1    7700 6250
	1    0    0    -1  
$EndComp
Connection ~ 7700 6250
$Comp
L power:GND #PWR048
U 1 1 62361245
P 8800 6150
F 0 "#PWR048" H 8800 5900 50  0001 C CNN
F 1 "GND" H 8805 5977 50  0000 C CNN
F 2 "" H 8800 6150 50  0001 C CNN
F 3 "" H 8800 6150 50  0001 C CNN
	1    8800 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR051
U 1 1 6236169C
P 9250 6150
F 0 "#PWR051" H 9250 5900 50  0001 C CNN
F 1 "GND" H 9255 5977 50  0000 C CNN
F 2 "" H 9250 6150 50  0001 C CNN
F 3 "" H 9250 6150 50  0001 C CNN
	1    9250 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR054
U 1 1 6236184C
P 9850 6050
F 0 "#PWR054" H 9850 5800 50  0001 C CNN
F 1 "GND" H 9855 5877 50  0000 C CNN
F 2 "" H 9850 6050 50  0001 C CNN
F 3 "" H 9850 6050 50  0001 C CNN
	1    9850 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 7500 4400 7500
Connection ~ 4400 7500
Wire Wire Line
	4400 7500 4450 7500
Wire Wire Line
	4400 6350 4400 7500
Wire Wire Line
	10050 1950 10050 2150
Wire Wire Line
	10050 2250 10050 2550
Wire Notes Line
	6400 550  9000 550 
Wire Notes Line
	6400 3700 9000 3700
$EndSCHEMATC
